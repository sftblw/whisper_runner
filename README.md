a tiny script to run [whisper ASR](https://github.com/openai/whisper)

1. install mamba
2. mamba env update -f environment.yml -p ./condaenv
3. mamba activate ./condaenv
4. python run.py