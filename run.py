import torch
import transformers.pipelines
import whisper
from pathlib import Path

import os
import tqdm

from typing import *
from dotenv import load_dotenv

import torch
from transformers import pipeline
from transformers.utils import is_flash_attn_2_available
import torchaudio


class ModelWrap:
    def __init__(self, model_name: str, device: str): pass
    def transcribe(self, file_name: str, lang: str = "en"): pass


class WhisperResultSegment(TypedDict):
    id: int
    start: Any
    end: Any
    text: str
    tokens: List[Any]
    words: List[Any]


class WhisperResult(TypedDict):
    text: str
    segments: List[WhisperResultSegment]
    language: Optional[str]


class ModelWhisper(ModelWrap):
    def __init__(self, model_name: str, device: str):
        super().__init__(model_name, device)
        self.model = whisper.load_model(name=model_name, device=device, download_root='./models_whisper')

    def transcribe(self, file_name: str, lang: str = "en") -> List[str]:
        result: WhisperResult = self.model.transcribe(file_name, **{
            'no_speech_threshold': 0.1,
            'fp16': True,
            'language': lang,
        })

        return "\n".join([seg["text"] for seg in result['segments']])


class ModelHuggingface(ModelWrap):
    """https://github.com/Vaibhavs10/insanely-fast-whisper"""

    pipe: transformers.pipelines.automatic_speech_recognition.AutomaticSpeechRecognitionPipeline

    def __init__(self, model_name: str, device: str):
        super().__init__(model_name, device)

        os.environ["HF_DATASETS_CACHE"] = "./models_whisper_hf"

        pipe = pipeline(
            "automatic-speech-recognition",
            model=model_name, # select checkpoint from https://huggingface.co/openai/whisper-large-v3#model-details
            torch_dtype=torch.float16,
            device=device, # or mps for Mac devices
            model_kwargs={"use_flash_attention_2": is_flash_attn_2_available()},
        )
        # if not is_flash_attn_2_available():
        #     # enable flash attention through pytorch sdpa
        #     pipe.model = pipe.model.to_bettertransformer()

        self.pipe = pipe

    def transcribe(self, file_name: str, lang: str = "en") -> List[str]:
        tokenizer: transformers.models.whisper.WhisperTokenizer = self.pipe.tokenizer
        tokenizer.set_prefix_tokens(language=lang)

        waveform = whisper.load_audio(file_name)

        outputs: Dict[str, Any] = self.pipe(
            {
                "sampling_rate": whisper.audio.SAMPLE_RATE,
                "raw": waveform
            },
            chunk_length_s=30,
            batch_size=12,
            return_timestamps=True,
            generate_kwargs={
                "language": lang,
            },
        )

        chunks: List[Dict[str, Any]] = outputs['chunks']

        return "\n".join([chunk['text'] for chunk in chunks])


def main():
    load_dotenv()
    
    lang = os.getenv("LANGUAGE") or "ko"
    model_kind = os.getenv("MODEL_KIND") or "large-v2"
    model_impl = os.getenv("MODEL_IMPL") or "huggingface"
    
    print('lang is ' + lang)

    print('loading model...')
    model_loader = ModelHuggingface if model_impl == "huggingface" else ModelWhisper
    model = model_loader(model_name=model_kind, device='cuda')

    basepath = Path('./todo')
    files = list(basepath.glob("*"))
    for filepath in tqdm.tqdm(files):
        try:
            filefullpath = str(filepath.absolute())
            print('file is ' + filefullpath)

            print('transcribing...')
            text = model.transcribe(filefullpath, lang)

            print('writing...')
            outfilepath = Path(filepath.parent, filepath.name + '.txt')
            outfilepath.write_text(text)

            print('ok')

            Path(filefullpath).rename(filefullpath.parent.parent / 'done' / filefullpath.name)
            outfilepath.rename(outfilepath.parent.parent / 'done' / outfilepath.name)

            print(str(outfilepath.absolute()))
        except Exception as ex:
            print(ex)
    
    if cur_platform == 'Windows':
        os.startfile(outfilepath.parent)
        os.startfile(outfilepath)


if __name__ == '__main__':
    import platform
    cur_platform = platform.system()

    if cur_platform == 'Windows':
        from wakepy import keepawake
        with keepawake(keep_screen_awake=False):
            main()
    else:
        main()
